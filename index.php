<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-07
 * Time: 16:52
 */

ini_set('error_reporting', E_ALL);

require 'vendor/autoload.php';

use OI\Telegram\Telegram;

//530729102:AAFOqb2rVBwRrb-66Z0injexVLF01Bi1Cbs

define('WEBHOOK_URL', 'https://tg.iank.nl/IanTicTacToeBot/index.php');

if (php_sapi_name() == 'cli') {
    // if run from console, set or delete webhook
    Telegram::apiRequest('setWebhook', array('url' => isset($argv[1]) && $argv[1] == 'delete' ? '' : WEBHOOK_URL));
    exit;
}

$content = file_get_contents("php://input");
$update = json_decode($content, true);

if (!$update) {
    // receive wrong update, must not happen
    exit;
}

$API = new Telegram("530729102:AAFOqb2rVBwRrb-66Z0injexVLF01Bi1Cbs");

$updateObj = new \OI\Telegram\Models\Update($update);

error_log(json_encode($updateObj->toJson()));


$keyboard = (new \OI\Telegram\Builders\ReplyKeyboardBuilder(2, 2))
    ->setButton(1, 0, "Up")
    ->setButton(0, 1, "Left")
    ->setButton(2, 1, "Right")
    ->setButton(1, 2, "Down")
    ->build();

$chatId = $updateObj->message->chat->id;
$API->sendMessage($chatId, "pizza", null, null, null, null, new \OI\Telegram\Models\ForceReply([
    "force_reply" => false
]));
