<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-11
 * Time: 00:37
 */

namespace OI\Telegram\Communication\Response;


use OI\Telegram\Models\Message;

class sendMessage
{

    /**
     * The returned message after sending it
     * @var Message
     */
    private $message;

    public function __construct($rawResponse)
    {
        $this->message = new Message($rawResponse);
    }

    public function getMessage() {
        return $this->message;
    }

}