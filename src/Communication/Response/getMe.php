<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-10
 * Time: 23:09
 */

namespace OI\Telegram\Communication\Response;

use OI\Telegram\Communication\IRequest;
use OI\Telegram\Communication\IResponse;
use OI\Telegram\Communication\Request;
use OI\Telegram\Communication\Response;
use OI\Telegram\Models\Implementation\ITelegramObject;
use OI\Telegram\Models\Implementation\TelegramObject;
use OI\Telegram\Models\User;

class getMe
{

    /**
     * The bot itself as an User instance
     * @var User
     */
    private $user;

    public function __construct($rawResponse)
    {
        $this->user = new User($rawResponse);
    }

    public function getUser() {
        return $this->user;
    }
}