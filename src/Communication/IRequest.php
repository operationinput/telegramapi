<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-10
 * Time: 23:14
 */

namespace OI\Telegram\Communication;

use OI\Telegram\Models\Implementation\ITelegramObject;

interface IRequest
{
    /**
     * IRequest constructor.
     * @param string $token
     */
    public function __construct(string $token);
}