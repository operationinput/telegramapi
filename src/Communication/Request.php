<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-10
 * Time: 22:30
 */

namespace OI\Telegram\Communication;

class Request
{

    /**
     * The base URL endpoint for the API
     * @var string
     */
    private static $apiUrl = "https://api.telegram.org";

    /**
     * Token used for accessing the HTTP API
     * @var string
     */
    private $apiToken;

    /**
     * Method the request will be executing
     * @var string
     */
    private $method;

    /**
     * Wether or not this is a get file request (file request uses custom URL)
     * @var bool
     */
    private $fileRequest = false;

    public function __construct($token, $method)
    {
        $this->apiToken = $token;
        $this->method = $method;
    }

    /**
     * Mark this request as a File Request
     */
    public function isFileRequest()
    {
        $this->fileRequest = true;
    }

    /**
     * Send the Request and get response
     * @param null $data Key value (sub) array added as JSON to the request
     * @return mixed
     */
    public function sendRequest($data = null)
    {
        if (!empty($data) && !is_array($data)) {
            error_log("Parameters must be an array\n");
            return null;
        }

        $handle = curl_init($this->getUrl());
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($handle, CURLOPT_TIMEOUT, 60);

        if ($data) {
            curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        }

        return self::exec_curl_request($handle);
    }

    /**
     * Internal function for executing the curl request
     * @param CURLHandle $handle The CURL handle object
     * @return mixed
     */
    private function exec_curl_request($handle)
    {
        $response = curl_exec($handle);

        if ($response === false) {
            $errno = curl_errno($handle);
            $error = curl_error($handle);
            error_log("Curl returned error $errno: $error\n");
            curl_close($handle);
            return null;
        }

        $http_code = intval(curl_getinfo($handle, CURLINFO_HTTP_CODE));
        curl_close($handle);

        if ($http_code >= 500) {
            sleep(10);
            return null;
        } else if ($http_code != 200) {
            $response = json_decode($response, true);
            error_log("Request has failed with error {$response['error_code']}: {$response['description']}\n");
            if ($http_code == 401) {
                throw new Exception('Invalid access token provided');
            }
            return null;
        } else {
            $response = json_decode($response, true);
            if (isset($response['description'])) {
                error_log("Request was successful: {$response['description']}\n");
            }
            $response = $response['result'];
        }

        return $response;
    }

    /**
     * Build the actual url for the API request
     * @return string
     */
    private function getUrl()
    {
        return self::$apiUrl . ($this->isFileRequest() ? "/file" : "") . "/bot"  . $this->apiToken . "/" . $this->method;
    }
}