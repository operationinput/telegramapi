<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-11
 * Time: 00:37
 */

namespace OI\Telegram\Communication\Request;


use OI\Telegram\Communication\IRequest;
use OI\Telegram\Communication\Request;

class sendMessage extends Request implements IRequest
{
    public function __construct(string $token)
    {
        parent::__construct($token, "sendMessage");
    }
}