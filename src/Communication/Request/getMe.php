<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-10
 * Time: 23:09
 */

namespace OI\Telegram\Communication\Request;

use OI\Telegram\Communication\IRequest;
use OI\Telegram\Communication\Request;

class getMe extends Request implements IRequest
{
    public function __construct(string $token)
    {
        parent::__construct($token, "getMe");
    }
}