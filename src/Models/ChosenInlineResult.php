<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 13:51
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * Represents a result of an inline query that was chosen by the user and sent to their chat partner.
 * @package OI\Telegram\Models
 */
class ChosenInlineResult extends TelegramObject
{

    /**
     * @var string
     * The unique identifier for the result that was chosen
     */
    public $resultId;

    /**
     * @var User
     * The user that chose the result
     */
    public $from;

    /**
     * @var Location
     * Optional. Sender location, only for bots that require user location
     */
    public $location;

    /**
     * @var string
     * Optional. Identifier of the sent inline message. Available only if there is an inline keyboard attached to the message. Will be also received in callback queries and can be used to edit the message.
     */
    public $inlineMessageId;

    /**
     * @var string
     * The query that was used to obtain the result
     */
    public $query;

    public function __construct($data)
    {
        $this->resultId = $this->t($data, "result_id");
        $this->from = $this->t($data, "from", User::class);
        $this->location = $this->t($data, "location", Location::class);
        $this->inlineMessageId = $this->t($data, "inline_message_id");
        $this->query = $this->t($data, "query");
    }

}