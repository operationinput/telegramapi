<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-11
 * Time: 12:26
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents one button of the reply keyboard. For simple text buttons String can be used instead of this object to specify text of the button. Optional fields are mutually exclusive.
 * @package OI\Telegram\Models
 */
class KeyboardButton extends TelegramObject
{

    /**
     * @var string
     * Text of the button. If none of the optional fields are used, it will be sent as a message when the button is pressed
     */
    public $text;

    /**
     * @var boolean
     * Optional. If True, the user's phone number will be sent as a contact when the button is pressed. Available in private chats only
     */
    public $requestContact;

    /**
     * @var boolean
     * Optional. If True, the user's current location will be sent when the button is pressed. Available in private chats only
     */
    public $requestLocation;

    public function __construct($data)
    {
        $this->text = $this->t($data, "text");
        $this->requestContact = $this->t($data, "request_contact");
        $this->requestLocation = $this->t($data, "request_location");
    }

    /**
     * Generate a button with the given data
     * @param string $text Text inside of the button
     * @param boolean $requestContact If the button should return a contact
     * @param boolean $requestLocation If the button should return a location
     * @return KeyboardButton
     */
    public static function createButton($text, $requestContact, $requestLocation)
    {
        return new self([
            "text" => $text,
            "request_contact" => $requestContact,
            "request_location" => $requestLocation
        ]);
    }

    /**
     * Generate a button without any items
     * @return KeyboardButton
     */
    public static function createBlankButton()
    {
        return new self([
            "text" => "　"
        ]);
    }

}