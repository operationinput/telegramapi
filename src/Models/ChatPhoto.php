<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-09
 * Time: 22:33
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents a chat photo.
 * @package OI\Telegram\Models
 */
class ChatPhoto extends TelegramObject
{

    /**
     * @var string
     * Unique file identifier of small (160x160) chat photo. This file_id can be used only for photo download.
     */
    public $smallFileId;

    /**
     * @var string
     * Unique file identifier of big (640x640) chat photo. This file_id can be used only for photo download.
     */
    public $bigFileId;

    public function __construct($data)
    {
        $this->smallFileId = $this->t($data, "small_file_id");
        $this->bigFileId = $this->t($data, "big_file_id");
    }

}