<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-11
 * Time: 11:55
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents an inline keyboard that appears right next to the message it belongs to.
 * @package OI\Telegram\Models
 */
class InlineKeyboardMarkup extends TelegramObject
{

    /**
     * Array of button rows, each represented by an Array of InlineKeyboardButton objects
     * @var InlineKeyboardButton[][]
     */
    public $inlineKeyboard;

    public function __construct($data)
    {
        $this->inlineKeyboard = $this->tSquared($data, "inline_keyboard", InlineKeyboardButton::class);
    }

}