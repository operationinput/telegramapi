<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 09:34
 */

namespace OI\Telegram\Models;

use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents a message.
 * @package OI\Telegram\Models
 */
class Message extends TelegramObject
{

    /**
     * @var integer
     * Unique message identifier inside this chat
     */
    public $messageId;

    /**
     * @var User;
     * Optional. Sender, empty for messages sent to channels
     */
    public $from;

    /**
     * @var DateTime
     * Date the message was sent in Unix time
     */
    public $date;

    /**
     * @var Chat
     * Conversation the message belongs to
     */
    public $chat;

    /**
     * @var User
     * Optional. For forwarded messages, sender of the original message
     */
    public $forwardFrom;

    /**
     * @var Chat
     * Optional. For messages forwarded from channels, information about the original channel
     */
    public $forwardFromChat;

    /**
     * @var integer
     * Optional. For messages forwarded from channels, information about the original channel
     */
    public $forwardFromMessageId;

    /**
     * @var string
     * Optional. For messages forwarded from channels, signature of the post author if present
     */
    public $forwardSignature;

    /**
     * @var DateTime
     * Optional. For forwarded messages, date the original message was sent in Unix time
     */
    public $forwardDate;

    /**
     * @var Message
     * Optional. For replies, the original message. Note that the Message object in this field will not contain further reply_to_message fields even if it itself is a reply.
     */
    public $replyToMessage;

    /**
     * @var DateTime
     * Optional. Date the message was last edited in Unix time
     */
    public $editDate;

    /**
     * @var string
     * Optional. The unique identifier of a media message group this message belongs to
     */
    public $mediaGroupId;

    /**
     * @var string
     * Optional. Signature of the post author for messages in channels
     */
    public $authorSignature;

    /**
     * @var string
     * Optional. For text messages, the actual UTF-8 text of the message, 0-4096 characters.
     */
    public $text;

    /**
     * @var MessageEntity[]
     * Optional. For text messages, special entities like usernames, URLs, bot commands, etc. that appear in the text
     */
    public $entities;

    /**
     * @var MessageEntity[]
     * Optional. For messages with a caption, special entities like usernames, URLs, bot commands, etc. that appear in the caption
     */
    public $captionEntities;

    /**
     * @var Audio
     * Optional. Message is an audio file, information about the file
     */
    public $audio;

    /**
     * @var Document
     * Optional. Message is a general file, information about the file
     */
    public $document;

    /**
     * @var Game
     * Optional. Message is a game, information about the game.
     */
    public $game;

    /**
     * @var PhotoSize[]
     * Optional. Message is a photo, available sizes of the photo
     */
    public $photo;

    /**
     * @var Sticker
     * Optional. Message is a sticker, information about the sticker
     */
    public $sticker;

    /**
     * @var Video
     * Optional. Message is a video, information about the video
     */
    public $video;

    /**
     * @var Voice
     * Optional. Message is a voice message, information about the file
     */
    public $voice;

    /**
     * @var VideoNote
     * Optional. Message is a video note, information about the video message
     */
    public $videoNote;

    /**
     * @var string
     * Optional. Caption for the audio, document, photo, video or voice, 0-200 characters
     */
    public $caption;

    /**
     * @var Contact
     * Optional. Message is a shared contact, information about the contact
     */
    public $contact;

    /**
     * @var Location
     * Optional. Message is a shared location, information about the location
     */
    public $location;

    /**
     * @var Venue
     * Optional. Message is a venue, information about the venue
     */
    public $venue;

    /**
     * @var User[]
     * Optional. New members that were added to the group or supergroup and information about them (the bot itself may be one of these members)
     */
    public $newChatMembers;

    /**
     * @var User
     * Optional. A member was removed from the group, information about them (this member may be the bot itself)
     */
    public $leftChatMember;

    /**
     * @var string
     * Optional. A chat title was changed to this value
     */
    public $newChatTitle;

    /**
     * @var PhotoSize[]
     * Optional. A chat photo was change to this value
     */
    public $newChatPhoto;

    /**
     * @var boolean
     * Optional. Service message: the chat photo was deleted
     */
    public $deleteChatPhoto;

    /**
     * @var boolean
     * Optional. Service message: the group has been created
     */
    public $groupChatCreated;

    /**
     * @var boolean
     * Optional. Service message: the supergroup has been created. This field can‘t be received in a message coming through updates, because bot can’t be a member of a supergroup when it is created. It can only be found in reply_to_message if someone replies to a very first message in a directly created supergroup.
     */
    public $supergroupChatCreated;

    /**
     * @var boolean
     * Optional. Service message: the channel has been created. This field can‘t be received in a message coming through updates, because bot can’t be a member of a channel when it is created. It can only be found in reply_to_message if someone replies to a very first message in a channel.
     */
    public $channelChatCreated;

    /**
     * @var integer
     * Optional. The group has been migrated to a supergroup with the specified identifier. This number may be greater than 32 bits and some programming languages may have difficulty/silent defects in interpreting it. But it is smaller than 52 bits, so a signed 64 bit integer or double-precision float type are safe for storing this identifier.
     */
    public $migrateToChatId;

    /**
     * @var integer
     * Optional. The supergroup has been migrated from a group with the specified identifier. This number may be greater than 32 bits and some programming languages may have difficulty/silent defects in interpreting it. But it is smaller than 52 bits, so a signed 64 bit integer or double-precision float type are safe for storing this identifier.
     */
    public $migrateFromChatId;

    /**
     * @var Message
     * Optional. Specified message was pinned. Note that the Message object in this field will not contain further reply_to_message fields even if it is itself a reply.
     */
    public $pinnedMessage;

    /**
     * @var Invoice
     * Optional. Message is an invoice for a payment, information about the invoice.
     */
    public $invoice;

    /**
     * @var SuccessfulPayment
     * Optional. Message is a service message about a successful payment, information about the payment.
     */
    public $successfulPayment;

    /**
     * @var string
     * Optional. The domain name of the website on which the user has logged in.
     */
    public $connectedWebsite;

    public function __construct($data)
    {
        $this->messageId = $this->t($data, "message_id");
        $this->from = $this->t($data, "from", User::class);
        $this->date = $this->t($data, "date", DateTime::class, false, "@");
        $this->chat = $this->t($data, "chat", Chat::class);
        $this->forwardFrom = $this->t($data, "forward_from", User::class);
        $this->forwardFromChat = $this->t($data, "forward_from_chat", Chat::class);
        $this->forwardFromMessageId = $this->t($data, "forward_from_message_id");
        $this->forwardSignature = $this->t($data, "forward_signature");
        $this->forwardDate = $this->t($data, "forward_date", DateTime::class, false, "@");
        $this->replyToMessage = $this->t($data, "reply_to_message", Message::class);
        $this->editDate = $this->t($data, "edit_date", DateTime::class, false, "@");
        $this->mediaGroupId = $this->t($data, "media_group_id");
        $this->authorSignature = $this->t($data, "author_signature");
        $this->text = $this->t($data, "text");
        $this->entities = $this->t($data, "entities", MessageEntity::class, true);
        $this->captionEntities = $this->t($data, "caption_entities", MessageEntity::class, true);
        $this->audio = $this->t($data, "audio", Audio::class);
        $this->document = $this->t($data, "document", Document::class);
        $this->game = $this->t($data, "game", Game::class);
        $this->photo = $this->t($data, "photo", PhotoSize::class, true);
        $this->sticker = $this->t($data, "sticker", Sticker::class);
        $this->video = $this->t($data, "video", Video::class);
        $this->voice = $this->t($data, "voice", Voice::class);
        $this->videoNote = $this->t($data, "video_note", VideoNote::class);
        $this->caption = $this->t($data, "caption");
        $this->contact = $this->t($data, "contact", Contact::class);
        $this->location = $this->t($data, "location", Location::class);
        $this->venue = $this->t($data, "venue", Venue::class);
        $this->newChatMembers = $this->t($data, "new_chat_members", User::class, true);
        $this->leftChatMember = $this->t($data, "left_chat_member", User::class);
        $this->newChatTitle = $this->t($data, "new_chat_title");
        $this->newChatPhoto = $this->t($data, "new_chat_photo", PhotoSize::class, true);
        $this->deleteChatPhoto = $this->t($data, "delete_chat_photo");
        $this->groupChatCreated = $this->t($data, "group_chat_created");
        $this->supergroupChatCreated = $this->t($data, "supergroup_chat_created");
        $this->channelChatCreated = $this->t($data, "pannel_chat_created");
        $this->migrateToChatId = $this->t($data, "migrate_to_chat_id");
        $this->migrateFromChatId = $this->t($data, "migrate_from_chat_id");
        $this->pinnedMessage = $this->t($data, "pinned_message", Message::class);
        $this->invoice = $this->t($data, "invoice", Invoice::class);
        $this->successfulPayment = $this->t($data, "successful_payment", SuccessfulPayment::class);
        $this->connectedWebsite = $this->t($data, "connected_website");
    }
}