<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 10:42
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents a video message (available in Telegram apps as of v.4.0).
 * @package OI\Telegram\Models
 */
class VideoNote extends TelegramObject
{

    /**
     * @var string
     * Unique identifier for this file
     */
    public $fileId;

    /**
     * @var integer
     * Video width and height as defined by sender
     */
    public $length;

    /**
     * @var integer
     * Duration of the video in seconds as defined by sender
     */
    public $duration;

    /**
     * @var PhotoSize
     * Optional. Video thumbnail
     */
    public $thumb;

    /**
     * @var integer
     * Optional. File size
     */
    public $fileSize;

    public function __construct($data)
    {
        $this->fileId = $this->t($data, "file_id");
        $this->length = $this->t($data, "length");
        $this->duration = $this->t($data, "duration");
        $this->thumb = $this->t($data, "thumb", PhotoSize::class);
        $this->fileSize = $this->t($data, "file_size");
    }

}