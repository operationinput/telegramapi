<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 10:41
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents one size of a photo or a file / sticker thumbnail.
 * @package OI\Telegram\Models
 */
class Audio extends TelegramObject
{

    /**
     * @var string
     * Unique identifier for this file
     */
    public $fileId;

    /**
     * @var integer
     * Duration of the audio in seconds as defined by sender
     */
    public $duration;

    /**
     * @var string
     * Optional. Performer of the audio as defined by sender or by audio tags
     */
    public $performer;

    /**
     * @var string
     * Optional. Title of the audio as defined by sender or by audio tags
     */
    public $title;

    /**
     * @var string
     * Optional. MIME type of the file as defined by sender
     */
    public $mimeType;

    /**
     * @var integer
     * Optional. File size
     */
    public $fileSize;

    public function __construct($data)
    {
        $this->fileId = $this->t($data, "file_id");
        $this->duration = $this->t($data, "duration");
        $this->performer = $this->t($data, "performer");
        $this->title = $this->t($data, "title");
        $this->mimeType = $this->t($data, "mime_type");
        $this->fileSize = $this->t($data, "file_size");
    }

}