<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 10:41
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents one special entity in a text message. For example, hashtags, usernames, URLs, etc.
 * @package OI\Telegram\Models
 */
class MessageEntity extends TelegramObject
{

    /**
     * @var string
     * Type of the entity. Can be mention (@username), hashtag, bot_command, url, email, bold (bold text), italic (italic text), code (monowidth string), pre (monowidth block), text_link (for clickable text URLs), text_mention (for users without usernames)
     */
    public $type;

    /**
     * @var integer
     * Offset in UTF-16 code units to the start of the entity
     */
    public $offset;

    /**
     * @var integer
     * Length of the entity in UTF-16 code units
     */
    public $length;

    /**
     * @var string
     * Optional. For “text_link” only, url that will be opened after user taps on the text
     */
    public $url;

    /**
     * @var User
     * Optional. For “text_mention” only, the mentioned user
     */
    public $user;

    public function __construct($data)
    {
        $this->type = $this->t($data, "type");
        $this->offset = $this->t($data, "offset");
        $this->length = $this->t($data, "length");
        $this->url = $this->t($data, "url");
        $this->user = $this->t($data, "user", User::class);
    }

}