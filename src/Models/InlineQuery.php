<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 13:49
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents an incoming inline query. When the user sends an empty query, your bot could return some default or trending results.
 * @package OI\Telegram\Models
 */
class InlineQuery extends TelegramObject
{

    /**
     * @var integer
     * Unique identifier for this query
     */
    public $id;

    /**
     * @var User
     * Sender
     */
    public $from;

    /**
     * @var Location
     * Optional. Sender location, only for bots that request user location
     */
    public $location;

    /**
     * @var string
     * Text of the query (up to 512 characters)
     */
    public $query;

    /**
     * @var string
     * Offset of the results to be returned, can be controlled by the bot
     */
    public $offset;

    public function __construct($data)
    {
        $this->id = $this->t($data, "id");
        $this->from = $this->t($data, "from", User::class);
        $this->location = $this->t($data, "location", Location::class);
        $this->query = $this->t($data, "query");
        $this->offset = $this->t($data, "offset");
    }

}