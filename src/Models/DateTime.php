<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-10
 * Time: 19:55
 */

namespace OI\Telegram\Models;

use DateTimeZone;
use OI\Telegram\Models\Implementation\ITelegramObject;

class DateTime extends \DateTime implements ITelegramObject
{

    public function __construct(string $time = 'now', DateTimeZone $timezone = null)
    {
        parent::__construct($time, $timezone);
    }

    function t($data, $key, $objectType = null, $isArray = false, $prefixValue = null)
    {
        throw new \Exception("Not implemented for DateTime");
    }

    function toJson()
    {
        return $this->getTimestamp();
    }
}