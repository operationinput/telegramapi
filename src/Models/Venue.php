<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 10:44
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents a venue.
 * @package OI\Telegram\Models
 */
class Venue extends TelegramObject
{

    /**
     * @var Location
     * Venue location
     */
    public $location;

    /**
     * @var string
     * Name of the venue
     */
    public $title;

    /**
     * @var string
     * Address of the venue
     */
    public $address;

    /**
     * @var string
     * Optional. Foursquare identifier of the venue
     */
    public $foursquareId;

    public function __construct($data)
    {
        $this->location = $this->t($data, "location", Location::class);
        $this->title = $this->t($data, "title");
        $this->address = $this->t($data, "address");
        $this->foursquareId = $this->t($data, "foursquare_id");
    }

}