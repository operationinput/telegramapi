<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 10:41
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents a general file (as opposed to photos, voice messages and audio files).
 * @package OI\Telegram\Models
 */
class Document extends TelegramObject
{

    /**
     * @var string
     * Unique file identifier
     */
    public $fileId;

    /**
     * @var PhotoSize
     * Optional. Document thumbnail as defined by sender
     */
    public $thumb;

    /**
     * @var string
     * Optional. Original filename as defined by sender
     */
    public $fileName;

    /**
     * @var string
     * Optional. MIME type of the file as defined by sender
     */
    public $mimeType;

    /**
     * @var integer
     * Optional. File size
     */
    public $fileSize;

    public function __construct($data)
    {
        $this->fileId = $this->t($data, "file_id");
        $this->thumb = $this->t($data, "thumb", PhotoSize::class);
        $this->fileName = $this->t($data, "file_name");
        $this->mimeType = $this->t($data, "mime_type");
        $this->fileSize = $this->t($data, "file_size");
    }

}