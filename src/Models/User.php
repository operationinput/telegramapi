<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 10:38
 */

namespace OI\Telegram\Models;

use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents a Telegram user or bot.
 * @package OI\Telegram\Models
 */
class User extends TelegramObject
{

    /**
     * @var integer
     * Unique identifier for this user or bot
     */
    public $id;

    /**
     * @var boolean
     * True, if this user is a bot
     */
    public $isBot;

    /**
     * @var string
     * User‘s or bot’s first name
     */
    public $firstName;

    /**
     * @var string
     * Optional. User‘s or bot’s last name
     */
    public $lastName;

    /**
     * @var string
     * Optional. User‘s or bot’s username
     */
    public $username;

    /**
     * @var string
     * Optional. IETF language tag of the user's language
     */
    public $languageCode;

    public function __construct($data)
    {
        $this->id = $this->t($data, "id");
        $this->isBot = $this->t($data, "is_bot");
        $this->firstName = $this->t($data, "first_name");
        $this->lastName = $this->t($data, "last_name");
        $this->username = $this->t($data, "username");
        $this->languageCode = $this->t($data, "language_code");
    }

}