<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 13:53
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object contains information about an incoming pre-checkout query.
 * @package OI\Telegram\Models
 */
class PreCheckoutQuery extends TelegramObject
{

    /**
     * @var string
     * Unique query identifier
     */
    public $id;

    /**
     * @var User
     * User who sent the query
     */
    public $from;

    /**
     * @var string
     * Three-letter ISO 4217 currency code
     */
    public $currency;

    /**
     * @var integer
     * Total price in the smallest units of the currency (integer, not float/double). For example, for a price of US$ 1.45 pass amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies).
     */
    public $totalAmount;

    /**
     * @var string
     * Bot specified invoice payload
     */
    public $invoicePayload;

    /**
     * @var string
     * Optional. Identifier of the shipping option chosen by the user
     */
    public $shippingOptionId;

    /**
     * @var OrderInfo
     * Optional. Order info provided by the user
     */
    public $orderInfo;

    public function __construct($data)
    {
        $this->id = $this->t($data, "id");
        $this->from = $this->t($data, "from", User::class);
        $this->currency = $this->t($data, "currency");
        $this->totalAmount = $this->t($data, "total_amount");
        $this->invoicePayload = $this->t($data, "invoice_payload");
        $this->shippingOptionId = $this->t($data, "shipping_option_id");
        $this->orderInfo = $this->t($data, "order_info", OrderInfo::class);
    }

}