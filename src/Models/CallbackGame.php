<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-11
 * Time: 12:11
 */

namespace OI\Telegram\Models;

use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * A placeholder, currently holds no information. Use BotFather to set up your game.
 * @package OI\Telegram\Models
 */
class CallbackGame extends TelegramObject
{

}