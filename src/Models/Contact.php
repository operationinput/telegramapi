<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 10:42
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents a phone contact.
 * @package OI\Telegram\Models
 */
class Contact extends TelegramObject
{

    /**
     * @var string
     * Contact's phone number
     */
    public $phoneNumber;

    /**
     * @var string
     * Contact's first name
     */
    public $firstName;

    /**
     * @var string
     * Optional. Contact's last name
     */
    public $lastName;

    /**
     * @var integer
     * Optional. Contact's user identifier in Telegram
     */
    public $userId;

    public function __construct($data)
    {
        $this->phoneNumber = $this->t($data, "phone_number");
        $this->firstName = $this->t($data, "first_name");
        $this->lastName = $this->t($data, "last_name");
        $this->userId = $this->t($data, "user_id");
    }

}