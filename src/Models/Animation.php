<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-10
 * Time: 00:22
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * You can provide an animation for your game so that it looks stylish in chats (check out Lumberjack for an example). This object represents an animation file to be displayed in the message containing a game.
 * @package OI\Telegram\Models
 */
class Animation extends  TelegramObject
{

    /**
     * @var string
     * Unique file identifier
     */
    public $fileId;

    /**
     * @var PhotoSize
     * Optional. Animation thumbnail as defined by sender
     */
    public $thumb;

    /**
     * @var string
     * Optional. Original animation filename as defined by sender
     */
    public $fileName;

    /**
     * @var string
     * Optional. MIME type of the file as defined by sender
     */
    public $mimeType;

    /**
     * @var integer
     * Optional. File size
     */
    public $fileSize;

    public function __construct($data)
    {
        $this->fileId = $this->t($data, "file_id");
        $this->thumb = $this->t($data, "thumb", PhotoSize::class);
        $this->fileName = $this->t($data, "file_name");
        $this->mimeType = $this->t($data, "mime_type");
        $this->fileSize = $this->t($data, "file_size");
    }

}