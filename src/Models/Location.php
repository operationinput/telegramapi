<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 10:44
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;


/**
 * This object represents a point on the map.
 * @package OI\Telegram\Models
 */
class Location extends TelegramObject
{

    /**
     * @var float
     * Longitude as defined by sender
     */
    public $longitude;

    /**
     * @var float
     * Latitude as defined by sender
     */
    public $latitude;

    public function __construct($data)
    {
        $this->longitude = $this->t($data, "longitude");
        $this->latitude = $this->t($data, "latitude");
    }

}