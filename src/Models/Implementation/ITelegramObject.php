<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-10
 * Time: 19:58
 */

namespace OI\Telegram\Models\Implementation;


interface ITelegramObject
{
    function t($data, $key, $objectType = null, $isArray = false, $prefixValue = null);
    function toJson();
}