<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 14:00
 */

namespace OI\Telegram\Models\Implementation;


class TelegramObject implements ITelegramObject
{
    /**
     * Verify and obtain data from dataset
     * @param array $data Data set
     * @param string $key Key for the data set
     * @param string $objectType String for the namespace of the class
     * @param bool $isArray Data set has multiple objects
     * @param null $prefixValue Added left to the value
     * @return null|array|object
     */
    public function t($data, $key, $objectType = null, $isArray = false, $prefixValue = null)
    {
        $value = $data[$key] ?? null;
        if (is_null($value))
            return $value;
        if ($isArray && is_array($value)) {
            $returnSet = [];
            foreach ($value as $single) {
                if (!is_null($prefixValue))
                    $single = $prefixValue . $single;
                $returnSet[] = is_null($objectType) ? $single : new $objectType($single);
            }
            return $returnSet;
        }
        if (!is_null($prefixValue))
            $value = $prefixValue . $value;
        return is_null($objectType) ? $value : new $objectType($value);
    }

    public function tSquared($data, $key, $objectType)
    {
        $value = $data[$key] ?? null;
        if (is_null($value))
            return $value;
        if (is_array($value)) {
            $returnSet = [];
            foreach ($value as $x => $row) {
                $returnSet[$x] = array();
                if (!is_array($row))
                    return [];
                foreach ($row as $y => $obj) {
                    $returnSet[$x][$y] = new $objectType($obj);
                }
            }
            return $returnSet;
        }
        return null;
    }

    /**
     * Convert the object to a JSon(-able) array
     * @return array
     */
    public function toJson()
    {
        $data = [];
        $fields = array_filter(get_object_vars($this), function ($value) {
            return !is_null($value);
        });
        foreach ($fields as $key => $field) {
            $cField = [];
            if (is_null($field)) continue;
            if (is_array($field)) {
                foreach ($field as $k1 => $v1) {
                    if (is_null($v1)) continue;
                    if (is_array($v1)) {
                        $cField[$k1] = [];
                        foreach ($v1 as $k2 => $v2) {
                            if (is_null($v2)) continue;
                            if (is_array($v2)) {
                                $cField[$k1][$k2] = [];
                                foreach($v2 as $k3 => $v3) {
                                    if(is_null($v3)) continue;
                                    $cField[$k1][$k2][$k3] = is_object($v3) ? $v3->toJson() : $v3;
                                }
                            } else {
                                $cField[$k1][$k2] = is_object($v2) ? $v2->toJson() : $v2;
                            }
                        }
                    } else {
                        $cField[$k1] = is_object($v1) ? $v1->toJson() : $v1;
                    }
                }
            }
            $data[$this->decamelize($key)] = is_object($field) ? $field->toJson() : (is_array($field) ? $cField : $field);
        }
        return $data;
    }

    /**
     * Convert strings from CamelCase to camel_case
     * @param string $input String that will convert to underscore
     * @return string
     */
    private function decamelize($input)
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
    }
}