<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-10
 * Time: 15:57
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object contains basic information about an invoice.
 * @package OI\Telegram\Models
 */
class MaskPosition extends TelegramObject
{

    /**
     * @var string
     * The part of the face relative to which the mask should be placed. One of “forehead”, “eyes”, “mouth”, or “chin”.
     */
    public $point;

    /**
     * @var float
     * The part of the face relative to which the mask should be placed. One of “forehead”, “eyes”, “mouth”, or “chin”.
     */
    public $xShift;

    /**
     * @var float
     * The part of the face relative to which the mask should be placed. One of “forehead”, “eyes”, “mouth”, or “chin”.
     */
    public $yShift;

    /**
     * @var float
     * Mask scaling coefficient. For example, 2.0 means double size.
     */
    public $scale;

    public function __construct($data)
    {
        $this->point = $this->t($data, "point");
        $this->xShift = $this->t($data, "x_shift");
        $this->yShift = $this->t($data, "y_shift");
        $this->scale = $this->t($data, "scale");
    }

}