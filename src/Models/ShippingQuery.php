<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 13:52
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object contains information about an incoming shipping query.
 * @package OI\Telegram\Models
 */
class ShippingQuery extends TelegramObject
{

    /**
     * @var string
     * Unique query identifier
     */
    public $id;

    /**
     * @var User
     * User who sent the query
     */
    public $from;

    /**
     * @var string
     * Bot specified invoice payload
     */
    public $invoicePayload;

    /**
     * @var ShippingAddress
     * User specified shipping address
     */
    public $shippingAddress;

    public function __construct($data)
    {
        $this->id = $this->t($data, "id");
        $this->from = $this->t($data, "from", User::class);
        $this->invoicePayload = $this->t($data, "invoice_payload");
        $this->shippingAddress = $this->t($data, "shipping_address", ShippingAddress::class);
    }

}