<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-09
 * Time: 00:33
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents a shipping address.
 * @package OI\Telegram\Models
 */
class ShippingAddress extends TelegramObject
{

    /**
     * @var string
     * ISO 3166-1 alpha-2 country code
     */
    public $countryCode;

    /**
     * @var string
     * State, if applicable
     */
    public $state;

    /**
     * @var string
     * City
     */
    public $city;

    /**
     * @var string
     * First line for the address
     */
    public $streetLine1;

    /**
     * @var string
     * Second line for the address
     */
    public $streetLine2;

    /**
     * @var string
     * Address post code
     */
    public $postCode;

    public function __construct($data)
    {
        $this->countryCode = $this->t($data, "country_code");
        $this->state = $this->t($data, "state");
        $this->city = $this->t($data, "city");
        $this->streetLine1 = $this->t($data, "street_line1");
        $this->streetLine2 = $this->t($data, "street_line2");
        $this->postCode = $this->t($data, "post_code");
    }

}