<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 10:42
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents a sticker.
 * @package OI\Telegram\Models
 */
class Sticker extends TelegramObject
{

    /**
     * @var string
     * Unique identifier for this file
     */
    public $fileId;

    /**
     * @var integer
     * Sticker width
     */
    public $width;

    /**
     * @var integer
     * Sticker height
     */
    public $height;

    /**
     * @var PhotoSize
     * Optional. Sticker thumbnail in the .webp or .jpg format
     */
    public $thumb;

    /**
     * @var string
     * Optional. Emoji associated with the sticker
     */
    public $emoji;

    /**
     * @var string
     * Optional. Name of the sticker set to which the sticker belongs
     */
    public $setName;

    /**
     * @var MaskPosition
     * Optional. For mask stickers, the position where the mask should be placed
     */
    public $maskPosition;

    /**
     * @var integer
     * Optional. File size
     */
    public $fileSize;

    public function __construct($data)
    {
        $this->fileId = $this->t($data, "file_id");
        $this->width = $this->t($data, "width");
        $this->height = $this->t($data, "height");
        $this->thumb = $this->t($data, "thumb", PhotoSize::class);
        $this->emoji = $this->t($data, "emoji");
        $this->setName = $this->t($data, "set_name");
        $this->maskPosition = $this->t($data, "mask_position", MaskPosition::class);
        $this->fileSize = $this->t($data, "file_size");
    }

}