<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 13:51
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents an incoming callback query from a callback button in an inline keyboard. If the button that originated the query was attached to a message sent by the bot, the field message will be present. If the button was attached to a message sent via the bot (in inline mode), the field inline_message_id will be present. Exactly one of the fields data or game_short_name will be present.
 * @package OI\Telegram\Models
 */
class CallbackQuery extends TelegramObject
{

    /**
     * @var string
     * Unique identifier for this query
     */
    public $id;

    /**
     * @var User
     * Sender
     */
    public $from;

    /**
     * @var Message
     * Optional. Message with the callback button that originated the query. Note that message content and message date will not be available if the message is too old
     */
    public $message;

    /**
     * @var string
     * Optional. Identifier of the message sent via the bot in inline mode, that originated the query.
     */
    public $inlineMessageId;

    /**
     * @var string
     * Global identifier, uniquely corresponding to the chat to which the message with the callback button was sent. Useful for high scores in games.
     */
    public $chatInstance;

    /**
     * @var string
     * Optional. Data associated with the callback button. Be aware that a bad client can send arbitrary data in this field.
     */
    public $data;

    /**
     * @var string
     * Optional. Short name of a Game to be returned, serves as the unique identifier for the game
     */
    public $gameShortName;

    public function __construct($data)
    {
        $this->id = $this->t($data, "id");
        $this->from = $this->t($data, "from", User::class);
        $this->message = $this->t($data, "message", Message::class);
        $this->inlineMessageId = $this->t($data, "inline_message_id");
        $this->chatInstance = $this->t($data, "chat_instance");
        $this->data = $this->t($data, "data");
        $this->gameShortName = $this->t($data, "game_short_name");
    }

}