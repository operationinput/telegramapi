<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-11
 * Time: 13:01
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * Upon receiving a message with this object, Telegram clients will display a reply interface to the user (act as if the user has selected the bot‘s message and tapped ’Reply'). This can be extremely useful if you want to create user-friendly step-by-step interfaces without having to sacrifice privacy mode.
 * @package OI\Telegram\Models
 */
class ForceReply extends TelegramObject
{

    /**
     * @var boolean
     * Shows reply interface to the user, as if they manually selected the bot‘s message and tapped ’Reply'
     */
    public $forceReply;

    /**
     * @var boolean
     * Optional. Use this parameter if you want to force reply from specific users only. Targets: 1) users that are @mentioned in the text of the Message object; 2) if the bot's message is a reply (has reply_to_message_id), sender of the original message.
     */
    public $selective;

    public function __construct($data)
    {
        $this->forceReply = $this->t($data, "force_reply");
        $this->selective = $this->t($data, "selective");
    }

}