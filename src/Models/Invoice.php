<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 10:44
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object contains basic information about an invoice.
 * @package OI\Telegram\Models
 */
class Invoice extends TelegramObject
{

    /**
     * @var string
     * Product name
     */
    public $title;

    /**
     * @var string
     * Product description
     */
    public $description;

    /**
     * @var string
     * Unique bot deep-linking parameter that can be used to generate this invoice
     */
    public $startParameter;

    /**
     * @var string
     * Three-letter ISO 4217 currency code
     */
    public $currency;

    /**
     * @var integer
     * Total price in the smallest units of the currency (integer, not float/double). For example, for a price of US$ 1.45 pass amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies).
     */
    public $totalAmount;

    public function __construct($data)
    {
        $this->title = $this->t($data, "title");
        $this->description = $this->t($data, "description");
        $this->startParameter = $this->t($data, "start_parameter");
        $this->currency = $this->t($data, "currency");
        $this->totalAmount = $this->t($data, "total_amount");
    }

}