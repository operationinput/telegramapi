<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 10:42
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents a video file.
 * @package OI\Telegram\Models
 */
class Video extends TelegramObject
{

    /**
     * @var string
     * Unique identifier for this file
     */
    public $fileId;

    /**
     * @var integer
     * Video width as defined by sender
     */
    public $width;

    /**
     * @var integer
     * Video width as defined by sender
     */
    public $height;

    /**
     * @var integer
     * Video width as defined by sender
     */
    public $duration;

    /**
     * @var PhotoSize
     * Optional. Video thumbnail
     */
    public $thumb;

    /**
     * @var string
     * Optional. Video thumbnail
     */
    public $mimeType;

    /**
     * @var integer
     * Optional. File size
     */
    public $fileSize;

    public function __construct($data)
    {
        $this->fileId = $this->t($data, "file_id");
        $this->width = $this->t($data, "width");
        $this->height = $this->t($data, "height");
        $this->duration = $this->t($data, "duration");
        $this->thumb = $this->t($data, "thumb", PhotoSize::class);
        $this->mimeType = $this->t($data, "mime_type");
        $this->fileSize = $this->t($data, "file_size");
    }

}