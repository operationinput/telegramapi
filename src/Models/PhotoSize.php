<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 10:41
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents one size of a photo or a file / sticker thumbnail.
 * @package OI\Telegram\Models
 */
class PhotoSize extends TelegramObject
{

    /**
     * @var string
     * Unique identifier for this file
     */
    public $fileId;

    /**
     * @var integer
     * Photo width
     */
    public $width;

    /**
     * @var integer
     * Photo height
     */
    public $height;

    /**
     * @var integer
     * Optional. File size
     */
    public $fileSize;

    public function __construct($data)
    {
        $this->fileId = $this->t($data, "file_id");
        $this->height = $this->t($data, "width");
        $this->width = $this->t($data, "height");
        $this->fileSize = $this->t($data, "file_size");
    }

}