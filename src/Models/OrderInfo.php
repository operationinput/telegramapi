<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-09
 * Time: 00:39
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents information about an order.
 * @package OI\Telegram\Models
 */
class OrderInfo extends TelegramObject
{

    /**
     * @var string
     * Optional. User name
     */
    public $name;

    /**
     * @var string
     * Optional. User's phone number
     */
    public $phoneNumber;

    /**
     * @var string
     * Optional. User email
     */
    public $email;

    /**
     * @var ShippingAddress
     * Optional. User shipping address
     */
    public $shippingAddress;

    public function __construct($data)
    {
        $this->name = $this->t($data, "name");
        $this->phoneNumber = $this->t($data, "phone_number");
        $this->email = $this->t($data, "email");
        $this->shippingAddress = $this->t($data, "shipping_address", ShippingAddress::class);
    }

}