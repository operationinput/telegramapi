<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 10:44
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object contains basic information about a successful payment.
 * @package OI\Telegram\Models
 */
class SuccessfulPayment extends TelegramObject
{

    /**
     * @var string
     * Three-letter ISO 4217 currency code
     */
    public $currency;

    /**
     * @var integer
     * Total price in the smallest units of the currency (integer, not float/double). For example, for a price of US$ 1.45 pass amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies).
     */
    public $totalAmount;

    /**
     * @var string
     * Bot specified invoice payload
     */
    public $invoicePayload;

    /**
     * @var string
     * Bot specified invoice payload
     */
    public $shippingOptionId;

    /**
     * @var OrderInfo
     * Bot specified invoice payload
     */
    public $orderInfo;

    /**
     * @var string
     * Bot specified invoice payload
     */
    public $telegramPaymentChargeId;

    /**
     * @var string
     * Provider payment identifier
     */
    public $providerPaymentChargeId;

    public function __construct($data)
    {
        $this->currency = $this->t($data, "currency");
        $this->totalAmount = $this->t($data, "total_amount");
        $this->invoicePayload = $this->t($data, "invoice_payload");
        $this->shippingOptionId = $this->t($data, "shipping_option_id");
        $this->orderInfo = $this->t($data, "order_info", OrderInfo::class);
        $this->telegramPaymentChargeId = $this->t($data, "telegram_payment_charge_id");
        $this->providerPaymentChargeId = $this->t($data, "provider_payment_charge_id");
    }

}