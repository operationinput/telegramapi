<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-11
 * Time: 11:56
 */

namespace OI\Telegram\Models;

use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents one button of an inline keyboard. You must use exactly one of the optional fields.
 * @package OI\Telegram\Models
 */
class InlineKeyboardButton extends TelegramObject
{

    /**
     * @var string
     * Label text on the button
     */
    public $text;

    /**
     * @var string
     * Optional. HTTP url to be opened when button is pressed
     */
    public $url;

    /**
     * @var string
     * Optional. Data to be sent in a callback query to the bot when button is pressed, 1-64 bytes
     */
    public $callbackData;

    /**
     * @var string
     * Optional. If set, pressing the button will prompt the user to select one of their chats, open that chat and insert the bot‘s username and the specified inline query in the input field. Can be empty, in which case just the bot’s username will be inserted.
     * Note: This offers an easy way for users to start using your bot in inline mode when they are currently in a private chat with it. Especially useful when combined with switch_pm… actions – in this case the user will be automatically returned to the chat they switched from, skipping the chat selection screen.
     */
    public $switchInlineQuery;

    /**
     * @var string
     * Optional. If set, pressing the button will insert the bot‘s username and the specified inline query in the current chat's input field. Can be empty, in which case only the bot’s username will be inserted.
     * This offers a quick way for the user to open your bot in inline mode in the same chat – good for selecting something from multiple options.
     */
    public $switchInlineQueryCurrentChat;

    /**
     * @var CallbackGame
     * Optional. Description of the game that will be launched when the user presses the button.
     * NOTE: This type of button must always be the first button in the first row.
     */
    public $callbackGame;

    /**
     * @var boolean
     * Optional. Specify True, to send a Pay button.
     * NOTE: This type of button must always be the first button in the first row.
     */
    public $pay;

    public function __construct($data)
    {
        $this->text = $this->t($data, "text");
        $this->url = $this->t($data, "url");
        $this->callbackData = $this->t($data, "callback_data");
        $this->switchInlineQuery = $this->t($data, "switch_inline_query");
        $this->switchInlineQueryCurrentChat = $this->t($data, "switch_inline_query_current_chat");
        $this->callbackGame = $this->t($data, "callback_game", CallbackGame::class);
        $this->pay = $this->t($data, "pay");
    }

    /**
     * Generate a button with the given data
     * @param string $text Text inside of the button
     * @param string $data Data that will be used for the specified method
     * @param string $method Action that will be executed on click (url, callback_data, switch_inline_query, switch_inline_query_current_chat, callback_game, pay)
     * @return InlineKeyboardButton
     */
    public static function createButton($text, $method, $data)
    {
        return new self([
            "text" => $text,
            $method => $data
        ]);
    }

    /**
     * Generate a button without any items
     * @return InlineKeyboardButton
     */
    public static function createBlankButton()
    {
        return new self([
            "text" => "　",
            "callback_data" => "."
        ]);
    }

}