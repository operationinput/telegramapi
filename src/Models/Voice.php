<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 10:42
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents a voice note.
 * @package OI\Telegram\Models
 */
class Voice extends TelegramObject
{

    /**
     * @var string
     * Unique identifier for this file
     */
    public $fileId;

    /**
     * @var integer
     * Duration of the audio in seconds as defined by sender
     */
    public $duration;

    /**
     * @var string
     * Optional. MIME type of the file as defined by sender
     */
    public $mimeType;

    /**
     * @var integer
     * Optional. File size
     */
    public $fileSize;

    public function __construct($data)
    {
        $this->fileId = $this->t($data, "file_id");
        $this->duration = $this->t($data, "duration");
        $this->mimeType = $this->t($data, "mime_type");
        $this->fileSize = $this->t($data, "file_size");
    }

}