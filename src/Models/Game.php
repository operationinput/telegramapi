<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-08
 * Time: 10:41
 */

namespace OI\Telegram\Models;


use OI\Telegram\Models\Implementation\TelegramObject;

/**
 * This object represents a game. Use BotFather to create and edit games, their short names will act as unique identifiers.
 * @package OI\Telegram\Models
 */
class Game extends TelegramObject
{

    /**
     * @var string
     * Title of the game
     */
    public $title;

    /**
     * @var string
     * Description of the game
     */
    public $description;

    /**
     * @var PhotoSize[]
     * Photo that will be displayed in the game message in chats.
     */
    public $photo;

    /**
     * @var string
     * Optional. Brief description of the game or high scores included in the game message. Can be automatically edited to include current high scores for the game when the bot calls setGameScore, or manually edited using editMessageText. 0-4096 characters.
     */
    public $text;

    /**
     * @var MessageEntity[]
     * Optional. Special entities that appear in text, such as usernames, URLs, bot commands, etc.
     */
    public $textEntities;

    /**
     * @var Animation
     * Optional. Animation that will be displayed in the game message in chats. Upload via BotFather
     */
    public $animation;

    public function __construct($data)
    {
        $this->title = $this->t($data, "title");
        $this->description = $this->t($data, "description");
        $this->photo = $this->t($data, "photo", PhotoSize::class, true);
        $this->text = $this->t($data, "text");
        $this->textEntities = $this->t($data, "text_entities", MessageEntity::class, true);
        $this->animation = $this->t($data, "animation", Animation::class);
    }

}