<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-07
 * Time: 16:43
 */

namespace OI\Telegram;

use OI\Telegram\Communication\Request;
use OI\Telegram\Communication\Response;
use OI\Telegram\Models\ForceReply;
use OI\Telegram\Models\InlineKeyboardMarkup;
use OI\Telegram\Models\ReplyKeyboardMarkup;

/**
 * Main API control class
 * @package OI\Telegram
 */
class Telegram
{

    /**
     * The token of the bot (excluding the bot beforehand)
     * @var string
     */
    private $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * A simple method for testing your bot's auth token. Requires no parameters. Returns basic information about the bot in form of a User object.
     * @return Response\getMe
     */
    public function getMe()
    {
        $request = new Request\getMe($this->token);
        return new Response\getMe($request->sendRequest());
    }

    /**
     * @param integer|string $chatId Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     * @param string $text
     * @param string $parseMode
     * @param boolean $disableWebPagePreview
     * @param boolean $disableNotification
     * @param integer $replyToMessageId
     * @param InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardMarkup|ForceReply $replyMarkup
     * @return Response\sendMessage
     */
    public function sendMessage($chatId, $text, $parseMode = null, $disableWebPagePreview = null, $disableNotification = null, $replyToMessageId = null, $replyMarkup = null)
    {
        $request = new Request\sendMessage($this->token);
        return new Response\sendMessage($request->sendRequest($this->p([
            "chat_id" => $chatId,
            "text" => $text,
            "parse_mode" => $parseMode,
            "disable_web_page_preview" => $disableWebPagePreview,
            "disable_notification" => $disableNotification,
            "reply_to_message_id" => $replyToMessageId,
            "reply_markup" => is_object($replyMarkup) ? $replyMarkup->toJson() : null
        ])));
    }

    /**
     * Filter the parameter array
     * @param array $data key value array that will be stripped
     * @return array
     */
    private function p($data)
    {
        return array_filter($data, function ($value) {
            return !is_null($value);
        });
    }

}
