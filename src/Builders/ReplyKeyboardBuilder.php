<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-13
 * Time: 17:38
 */

namespace OI\Telegram\Builders;


use OI\Telegram\Models\KeyboardButton;
use OI\Telegram\Models\ReplyKeyboardMarkup;

class ReplyKeyboardBuilder
{

    /**
     * They keyboard data that will be injected into InlineKeyboardMarkup
     * @var KeyboardButton[][]
     */
    private $keyboard;

    /**
     * The maximum index of columns
     * @var integer
     */
    private $maxX;

    /**
     * The maximum index of rows
     * @var integer
     */
    private $maxY;

    public function __construct($maxX, $maxY)
    {
        $this->maxX = $maxX;
        $this->maxY = $maxY;
        $this->fillMap();
    }

    /**
     * Create the map area
     */
    private function fillMap()
    {
        for ($i = 0; $i <= $this->maxY; $i++) {
            if (!isset($this->keyboard[$i])) $this->keyboard[$i] = [];
            for ($i2 = 0; $i2 <= $this->maxX; $i2++) {
                $this->keyboard[$i][$i2] = KeyboardButton::createBlankButton();
            }
        }
    }

    /**
     * Set a button inside of the keyboard field
     * @param int $x The X location of the button
     * @param int $y The Y location of the button
     * @param string $text Text inside of the button
     * @param boolean $requestContact If the button should return a contact
     * @param boolean $requestLocation If the button should return a location
     * @return ReplyKeyboardBuilder
     */
    public function setButton($x, $y, $text, $requestContact = false, $requestLocation = false)
    {
        $this->createLocation($x, $y);
        $this->keyboard[$y][$x] = $this->generateButton($text, $requestContact, $requestLocation);
        return $this;
    }

    /**
     * Generate the keyboard from the set buttons
     * @param bool $resizeKeyboard
     * @param bool $oneTimeKeyboard
     * @param bool $selective
     * @return ReplyKeyboardMarkup
     */
    public function build($resizeKeyboard = false, $oneTimeKeyboard = true, $selective = false)
    {
        $keyboard = new ReplyKeyboardMarkup([
            "resize_keyboard" => $resizeKeyboard,
            "one_time_keyboard" => $oneTimeKeyboard,
            "selective" => $selective
        ]);
        $keyboard->keyboard = $this->keyboard;
        return $keyboard;
    }

    /**
     * Generate a button with the given data
     * @param string $text Text inside of the button
     * @param boolean $requestContact If the button should return a contact
     * @param boolean $requestLocation If the button should return a location
     * @return KeyboardButton
     */
    private function generateButton($text, $requestContact, $requestLocation)
    {
        return KeyboardButton::createButton($text, $requestContact, $requestLocation);
    }

    /**
     *
     * @param int $x The X location of the button
     * @param int $y The Y location of the button
     */
    private function createLocation($x, $y)
    {
        if ($x < 0 || $y < 0) {
            throw new \OutOfRangeException("The location [$x,$y] is below 0");
        }
        if ($x > $this->maxX || $y > $this->maxY) {
            throw new \OutOfRangeException("The location [$x,$y] is above given maximum");
        }

        if (!isset($this->keyboard[$y])) {
            for ($i = 0; $i <= $y; $i++) {
                if (!isset($this->keyboard[$i])) $this->keyboard[$i] = [];
            }
        }
        if (!isset($this->keyboard[$y][$x])) {
            for ($i = 0; $i <= $x; $i++) {
                if (isset($this->keyboard[$y][$i])) continue;
                $this->keyboard[$y][$i] = KeyboardButton::createBlankButton();
            }
        }
    }
}