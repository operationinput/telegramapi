<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2018-03-11
 * Time: 13:44
 */

namespace OI\Telegram\Builders;


use OI\Telegram\Models\InlineKeyboardButton;
use OI\Telegram\Models\InlineKeyboardMarkup;

/**
 * Class InlineKeyboardBuilder
 * @package OI\Telegram\Builders
 * @TODO Handle callback_game & pay method
 */
class InlineKeyboardBuilder
{

    /**
     * They keyboard data that will be injected into InlineKeyboardMarkup
     * @var InlineKeyboardButton[][]
     */
    private $keyboard;

    /**
     * List of allowed methods for this keyboard
     * @var array
     */
    private $allowedMethods = [
        "url",
        "callback_data",
        "switch_inline_query",
        "switch_inline_query_current_chat",
        "callback_game",
        "pay"
    ];


    /**
     * The maximum index of columns
     * @var integer
     */
    private $maxX;

    /**
     * The maximum index of rows
     * @var integer
     */
    private $maxY;

    public function __construct($maxX, $maxY)
    {
        $this->maxX = $maxX;
        $this->maxY = $maxY;
        $this->fillMap();
    }

    /**
     * Create the map area
     */
    private function fillMap()
    {
        for ($i = 0; $i <= $this->maxY; $i++) {
            if (!isset($this->keyboard[$i])) $this->keyboard[$i] = [];
            for($i2 = 0; $i2 <= $this->maxX; $i2++) {
                $this->keyboard[$i][$i2] = InlineKeyboardButton::createBlankButton();
            }
        }
    }

    /**
     * Set a button inside of the keyboard field
     * @param int $x The X location of the button
     * @param int $y The Y location of the button
     * @param string $text Text inside of the button
     * @param string $data Data that will be used for the specified method
     * @param string $method Action that will be executed on click (url, callback_data, switch_inline_query, switch_inline_query_current_chat, callback_game, pay)
     * @return InlineKeyboardBuilder
     * @throws \OutOfRangeException
     */
    public function setButton($x, $y, $text, $method, $data)
    {
        $this->createLocation($x, $y);
        $this->keyboard[$y][$x] = $this->generateButton($text, $method, $data);
        return $this;
    }

    /**
     * Generate the keyboard from the set buttons
     * @return InlineKeyboardMarkup
     */
    public function build()
    {
        $keyboard = new InlineKeyboardMarkup([]);
        $keyboard->inlineKeyboard = $this->keyboard;
        return $keyboard;
    }

    /**
     * Generate a button with the given data
     * @param string $text Text inside of the button
     * @param string $data Data that will be used for the specified method
     * @param string $method Action that will be executed on click (url, callback_data, switch_inline_query, switch_inline_query_current_chat, callback_game, pay)
     * @return InlineKeyboardButton
     */
    private function generateButton($text, $method, $data)
    {
        if (!in_array($method, $this->allowedMethods)) {
            throw new \UnexpectedValueException("Method '$method' is not available");
        }
        return InlineKeyboardButton::createButton($text, $method, $data);
    }

    /**
     *
     * @param int $x The X location of the button
     * @param int $y The Y location of the button
     */
    private function createLocation($x, $y)
    {
        if ($x < 0 || $y < 0) {
            throw new \OutOfRangeException("The location [$x,$y] is below 0");
        }
        if ($x > $this->maxX || $y > $this->maxY) {
            throw new \OutOfRangeException("The location [$x,$y] is above given maximum");
        }

        if (!isset($this->keyboard[$y])) {
            for ($i = 0; $i <= $y; $i++) {
                if (!isset($this->keyboard[$i])) $this->keyboard[$i] = [];
            }
        }
        if (!isset($this->keyboard[$y][$x])) {
            for ($i = 0; $i <= $x; $i++) {
                if(isset($this->keyboard[$y][$i])) continue;
                $this->keyboard[$y][$i] = InlineKeyboardButton::createBlankButton();
            }
        }
    }

}